from django.conf.urls import url

from . import views

app_name = 'diary'
urlpatterns = [
    url(r'^$', views.login_view, name='login'),
    url(r'^home/$', views.home_view, name='home'),
    url(r'^home/marks$', views.marks_view, name='marks'),
    url(r'^home/frequency$', views.frequency_view, name='frequency'),
    url(r'^home/students$', views.students_view, name='students'),
    url(r'^home/teachers$', views.teachers_view, name='teachers'),
    url(r'^home/marks_t$', views.marks_t_view, name='marks_t'),
    url(r'^home/frequency_t$', views.frequency_t_view, name='frequency_t'),
]
