# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Subject(models.Model): # Klasa tworząca tabele przedmiotu za pośrednictwem frameworku django  dla bazy danych SQLite
    name = models.CharField(max_length = 30, null=True)  # pole okreslające nazwe
    def __str__(self):    # metoda zwracająca zawartość pola
        return self.name

class Mark(models.Model):# Klasa tworząca tabele oceny za pośrednictwem frameworku django  dla bazy danych SQLite
    student = models.ForeignKey(User, on_delete=models.CASCADE, null=True) # relacja pomiędzy parą tabel
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, null=True) # relacja pomiędzy parą tabel
    mark = models.IntegerField(choices=( #pole ocen
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
        (6, '6'),
                                       ))
    comment = models.CharField(max_length = 50, null=True)
    def __str__(self):# metoda zwracająca zawartość pól
        return self.student.__str__() + " " + self.subject.__str__() + " " + str(self.mark) + " " + self.comment

class Frequency(models.Model):# Klasa tworząca tabele frekwencji za pośrednictwem frameworku django  dla bazy danych SQLite
    student = models.ForeignKey(User, on_delete=models.CASCADE, null=True) # relacja pomiędzy parą tabel
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, null=True)# relacja pomiędzy parą tabel
    date = models.DateField() #pole określające date
    present = models.BooleanField() # pole okreslajace obecność
    def __str__(self): # metoda zwracająca zawartość pól
        return self.student.__str__() + " " + self.subject.__str__() + " " + str(self.date) + " " + str(self.present)
