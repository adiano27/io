# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from .forms import LoginForm
from django.contrib.auth.decorators import login_required
from django.conf.urls.static import static
from .models import Mark, Subject, Frequency
from datetime import date
from django.utils.dateparse import parse_date
 
# Create your views here.

# def login_view(request):
#     if request.method == "POST":
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(request, username=username, password=password)
#         if user is not None:
#             login(request, user)
#             return render(request, 'home.html')
#         else:
#             return HttpResponse('Log in error!')
#     else:
#         return render(request, 'diary/login.html')

@login_required
def home_view(request): #metoda obsługująca strone główną
    return render(request, 'diary/base.html') #zwrócenie do templates

def login_view(request): #metoda obsługująca logowanie
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username'] # pobranie loginu z  formularza
            password = form.cleaned_data['password'] # pobranie hasła z formularza
            user = authenticate(request, username=username, password=password) # autoryzacja
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse('diary:home'))
            else:
                form = LoginForm()
    else:
        form = LoginForm()

    return render(request, 'diary/login.html', {'form':form}) #zwrócenie do formularza


def marks_view(request): # metoda obslugująca wyswietlanie ocen studentów
    marks = Mark.objects.filter(student = request.user) # pobranie z modelu ocen
    subjects = Subject.objects.all()       # pobranie z modelu przedmiotów
    context = {
        'marks':marks,
        'subjects':subjects,
    }
    return render(request, 'diary/marks.html', context) #zwrócenie do templates

def frequency_view(request): # metoda obsługująca wyświetlanie frekwencji  dla ucznia
    frequency = Frequency.objects.filter(student = request.user) # pobranie z modelu obecności
    today = date.today()#pobranie daty
    context = {
        'today':today,
        'frequency':frequency,
    }
    return render(request, 'diary/frequency.html', context) #zwrócenie do templates
   
def students_view(request):# metoda obsługująca wyśweitlanie listy studentów
    students = User.objects.filter(groups__name = "Students") # pobranie listy sudentów
    return render(request, 'diary/students.html', {'students':students})#zwrócenie do templates

def teachers_view(request): #metoda obsługująca wywietlanie lisy nayczycieli
    teachers = User.objects.filter(groups__name = "Teachers")   # pobranie listy nauczycieli
    return render(request, 'diary/teachers.html', {'teachers':teachers})#zwrócenie do templates

def marks_t_view(request): #wyswietlanie ocen dla widoku nauczyciela
    if request.method == "POST":
        student = request.POST['students']# wyswietlanie listy
        student = User.objects.get(username=student)# wybór z listy
        subject = request.POST['subjects']# wyswietlanie listy
        subject = Subject.objects.get(name=subject)# wybór z listy
        mark = int(request.POST['mark'])# wybór z listy
        comment = request.POST['comment']# pobranie komentarza z formularzu
        Mark.objects.create(student=student,subject=subject,mark=mark,comment=comment)# stworzenie wpisu do bazy
        students = User.objects.filter(groups__name = "Students")#przeszukanie bazy
        subjects = Subject.objects.all()#przeszukanie bazy
        context = {
            'students':students,
            'subjects':subjects,
        }
        return render(request, 'diary/marks_t.html', context)
    else:
        students = User.objects.filter(groups__name = "Students")
        subjects = Subject.objects.all()
        context = {
            'students':students,
            'subjects':subjects,
        }
        return render(request, 'diary/marks_t.html',context)#zwrócenie do templates

def frequency_t_view(request): #wyswietlanie frekwencji dla widoku nauczyciela
    if request.method == "POST":
        student = request.POST['students'] # wyswietlanie listy
        student = User.objects.get(username=student)# wybór z listy
        subject = request.POST['subjects']# wyswietlanie listy
        subject = Subject.objects.get(name=subject)# wybór z listy
        date_str = request.POST['date']# wyswietlanie listy
        date = parse_date(date_str)#pobranie z formularza
        present = request.POST['present']# wyswietlanie listy
        if present == "obecny":
            isPresent = True
        else:
            isPresent = False
        Frequency.objects.create(student=student,subject=subject,date=date,present=isPresent)# stworzenie wpisu do bazy
        students = User.objects.filter(groups__name = "Students")# pzeszukanie bazy
        subjects = Subject.objects.all() # przeszukanie bazy
        context = {
            'students':students,
            'subjects':subjects,
        }
        return render(request, 'diary/frequency_t.html', context) # zwrócenie do templates
    else:
        is_counted = False
        students = User.objects.filter(groups__name = "Students")# przeszukanie bazy
        subjects = Subject.objects.all()# przeszukanie bazy
        context = {
            'students':students,
            'subjects':subjects,
        }
        return render(request, 'diary/frequency_t.html',context) #zwrócenie do templates